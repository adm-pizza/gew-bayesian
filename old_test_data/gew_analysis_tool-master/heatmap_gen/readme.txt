Converts form csv to heatmap

Dependencies:
python 2.7 
numpy 
matplotlib 
scipy 

How to call 
python getPolar.py <arg name>
 
<arg name assumes that csv file is in folder called csv>

sample call:
python getPolar_fin.py s_e2

getPolar.py
- csv
-- s_e2.csv
- figures
-- s_e2.png
Saves image file in folder call figures
