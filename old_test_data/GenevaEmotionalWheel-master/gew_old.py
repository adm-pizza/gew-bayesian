# embedding_in_qt5.py --- Simple Qt5 application embedding matplotlib canvases
#
# Copyright (C) 2005 Florent Rougon
#               2006 Darren Dale
#               2015 Jens H Nielsen
#
# This file is an example program for matplotlib. It may be used and
# modified with no restriction; raw copies as well as modified versions
# may be distributed without limitation.

from __future__ import unicode_literals
import sys
import os
import random
import matplotlib
# Make sure that we are using QT5
matplotlib.use('Qt5Agg')
from PyQt5 import QtCore, QtWidgets, QtGui
from PyQt5.QtCore import *
from PyQt5.QtWidgets import *
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.figure import Figure
import numpy as np
import math as m
from ast import literal_eval
from matplotlib.widgets import Button
from matplotlib.widgets import TextBox
import matplotlib.pyplot as plt
import datetime
import time



class Wheel(FigureCanvas):
    """Ultimately, this is a QWidget (as well as a FigureCanvasAgg, etc.)."""

    def __init__(self,filename, emotions, parent=None, width=7, height=7, dpi=100, nEmotions=20, amplitude=5):



        #The main figure, we set a default size we know look good
        self.fig = plt.figure(figsize=(width,height), dpi = dpi)
        #
        #the file for the output
        self.outputFile = open(filename, 'w')
        self.outputFile.write("\"Name\",\"Id\",\"Counter\",\"Emotion\",\"Intensity\",\"Other\"\n")
        self.emotions=emotions # array of emotions
        self.colors= ['grey', 'lightgrey']

        self.otherEmotion = ""
        self.lastClicked = None;
        self.fig.canvas.mpl_disconnect(self.fig.canvas.manager.key_press_handler_id)
        FigureCanvas.__init__(self, self.fig)
        self.setParent(parent)
        self.createAxes(nEmotions, amplitude)


    def __del__(self):
        self.outputFile.close()



    def createWheel(self, n, l):
        #create only one subplot
        ax = self.fig.add_subplot(1,1,1)
        #remove the axis, doesn't work with mpld3
        ax.axis('off')
        ax.set_title("Geneva emotion wheel")
        #ax.set_autoscale_on(False)
        plt.margins(0.17, 0.17)
        #add the center point for no emotions
        line, = ax.plot(0,0,'o', ms=40, color= 'grey',picker=20, label=(0,0))
        bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
        ax.text(0, 0, "None", ha="center", va="center", size=8,bbox=bbox_props)

        #add all the point for the various emotions
        for i in range(0, n):
            for j in range(2,l+3):
                x= m.pow(j/2,1.2)*m.cos(m.radians(360/(n*2) + (360/(n))*i))#formula that ensures the distances is greater when the points are bigger
                y= m.pow(j/2,1.2)*m.sin(m.radians(360/(n*2) + (360/(n))*i))
                if j is not l+2:
                    line, = ax.plot(x,y,'o', ms=(j)*3, picker=(j)*1.5, color= self.colors[i%2], label=(i+1,j-1))#creating the clickable dots,
                if j is l+2:
                    x= m.pow((j+.5)/2,1.2)*m.cos(m.radians(360/(n*2) + (360/(n))*i))
                    y= m.pow((j+.5)/2,1.2)*m.sin(m.radians(360/(n*2) + (360/(n))*i))
                    bbox_props = dict(boxstyle="round", fc="w", ec="0.5", alpha=0.9)
                    ax.text(x, y, self.emotions[i], ha="center", va="center", size=8,
                            bbox=bbox_props)
        self.fig.canvas.mpl_connect('pick_event', self.onpick)


    def createAxes(self, n, l):
        self.createWheel(n, l)

    def textChange(self, text):
        if(self.lastClicked!= None):
            color = (literal_eval(self.lastClicked.get_label())[0]-1)%2
            self.lastClicked.set_color(self.colors[color])
            self.lastClicked=None
            self.fig.canvas.draw()
        self.otherEmotion = text


    def onpick(self, event):
        #choice has been changed, set the last one's color back to grey
        if(self.lastClicked!= None):
            color = (literal_eval(self.lastClicked.get_label())[0]-1)%2
            self.lastClicked.set_color(self.colors[color])
        #get the point that has been clicked
        self.lastClicked = event.artist
        #change the color of the clicked point to red
        self.lastClicked.set_color('red')
        self.fig.canvas.draw()
        self.otherEmotion = ""

    def confirm(self, name, idNum, counter, event=None):
        if(self.lastClicked!= None):
            label = literal_eval(self.lastClicked.get_label())
            self.otherEmotion=""
            self.save(label, name, idNum, counter)
            #reset the lastClicked and it's color
            color = (label[0]-1)%2
            self.lastClicked.set_color(self.colors[color])
            self.lastClicked = None
            self.fig.canvas.draw()
            return True
        elif(self.otherEmotion!=""):
            label = ("","")
            self.save(label, name, idNum, counter)
            return True
        return False

    def save(self, label, name, idNum, counter):
        (emotion, intensity) = label

        self.outputFile.write(name+ "," + str(idNum) + ","+ str(counter)+ "," + str(emotion)+","+str(intensity) + "," + self.otherEmotion + "\n")



#        self.fig.canvas.draw()






class ApplicationWindow(QtWidgets.QMainWindow):


    def __init__(self):
        self.numberOfTests = 5
        QtWidgets.QMainWindow.__init__(self)
        self.setAttribute(QtCore.Qt.WA_DeleteOnClose)
        self.setWindowTitle("Geneva Emotion Wheel")
        self.main_widget = QtWidgets.QWidget(self)
        self.counter=self.numberOfTests
        self.wheel = None

        #l = QtWidgets.QVBoxLayout(self.main_widget)
        #self.sc = MyMplCanvas( filename, emotions, self.main_widget, width=5, height=4, dpi=100)
        #l.addWidget(self.wheel)
        self.main_widget.setFocus()
        self.setCentralWidget(self.main_widget)
        self.box = QtWidgets.QVBoxLayout(self.main_widget)
        self.id = 0
        self.name = ""





    def uberconfirm(self):
        if(self.wheel.confirm(self.name, self.id,self.numberOfTests+1-self.counter)):
            self.counter = self.counter-1
        if (self.counter is 0):
            self.swapWheel()
        self.counterLabel.setText("Counter: "+str(self.numberOfTests+1-self.counter))
        self.other.clear()

    def confirmSplash(self):
        self.name = self.nameLine.text()
        self.id = self.id+1
        self.counter=self.numberOfTests
        self.swapWheel()
        self.nameLine.clear()
        self.counterLabel.setText("Counter: "+str(self.numberOfTests+1-self.counter))
        self.other.clear()

    def addWheel(self, filename, emotions, nEmotions, intensity):

        self.wheel = Wheel(filename, emotions, self.main_widget, 35, 40, 50, nEmotions,intensity)
        #self.wheel.setFixedSize(500,500)
        self.box.addWidget(self.wheel)
        self.horizontalOther = QHBoxLayout()
        self.other = QLineEdit(self)
        #self.other.move(50, 500)
        #self.other.resize(280,40)
        self.other.textChanged.connect(self.wheel.textChange)
        self.otherlabel = QLabel("Other:")
        self.horizontalOther.addWidget(self.otherlabel)
        self.horizontalOther.addWidget(self.other)
        #self.box.addWidget("Other",self.other)
        self.box.addLayout(self.horizontalOther)
        self.confirm = QPushButton('confirm', self)
        self.confirm.setToolTip('Confirm')
        #self.confirm.move(500,400)
        self.confirm.clicked.connect(self.uberconfirm)
        self.box.addWidget(self.confirm)
        self.counterLabel = QLabel("Counter: " + str(self.numberOfTests+1-self.counter))
        self.box.addWidget(self.counterLabel)
        self.wheel.setHidden(True)#it's hidden at the start cuz the splash screen is there
        self.other.setHidden(True)#it's hidden at the start cuz the splash screen is there
        self.confirm.setHidden(True)#it's hidden at the start cuz the splash screen is there

        self.counterLabel.setHidden(True)
        self.otherlabel.setHidden(True)#it's hidden at the start cuz the splash screen is there


    def swapWheel(self):
        self.wheel.setHidden(not self.wheel.isHidden())
        self.other.setHidden(not self.other.isHidden())
        self.confirm.setHidden(not self.confirm.isHidden())
        self.nameLabel.setHidden(not self.nameLine.isHidden())
        self.nameLine.setHidden(not self.nameLine.isHidden())
        self.yesButton.setHidden(not self.yesButton.isHidden())
        self.noButton.setHidden(not self.noButton.isHidden())
        self.counterLabel.setHidden(not self.counterLabel.isHidden())
        self.otherlabel.setHidden(not self.otherlabel.isHidden())
        self.nLabel.setHidden(not self.nLabel.isHidden())


    def addIntro(self):
        self.nameLabel = QLabel("Do you agree to have your answer recorded?")
        self.horizontalName = QHBoxLayout()
        self.horizontalButtons = QHBoxLayout()
        self.nLabel = QLabel("Name:")
        self.nameLine = QLineEdit()
        self.horizontalName.addWidget(self.nLabel)
        self.horizontalName.addWidget(self.nameLine)
        self.yesButton = QPushButton("Yes")
        self.noButton = QPushButton("No")
        self.horizontalButtons.addWidget(self.yesButton)
        self.horizontalButtons.addWidget(self.noButton)

        self.box.addWidget(self.nameLabel)
        self.box.addLayout(self.horizontalButtons)
        self.box.addLayout(self.horizontalName)

        self.nameLabel.setHidden(False)
        self.nameLine.setHidden(False)
        self.yesButton.setHidden(False)
        self.noButton.setHidden(False)
        self.nLabel.setHidden(False)
        self.yesButton.clicked.connect(self.confirmSplash)

    def closeEvent(self, ce):
        self.close()



if __name__ == '__main__':
    emotions = ["Pleasure","Joy","Pride","Amusement","Interest","Anger","Hate","Contempt","Disgust","Fear","Disappointment","Shame","Regret","Guilt","Sadness","Compassion","Relief","Admiration","Love","Contentment"]
    n=len(emotions) # number of different emotions
    l=5 # degrees of amplitude of emotions
    now = datetime.datetime.now()
    qApp = QtWidgets.QApplication(sys.argv)
    aw = ApplicationWindow()
    aw.resize(1000, 1000)
    aw.show()
    aw.addWheel(now.strftime("%Y-%m-%d-%H-%M-%S.csv"), emotions, n, l)
    aw.addIntro()
    aw.swapWheel()
    aw.swapWheel()

    sys.exit(qApp.exec_())
