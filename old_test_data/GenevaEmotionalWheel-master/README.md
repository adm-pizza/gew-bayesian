# GenevaEmotionalWheel
A simple python implementation of the geneva emotion wheel (https://www.affective-sciences.org/gew/) which allows users to put in emotion levels and allowing to modify the wheel parameters. Outputting in csv format.
