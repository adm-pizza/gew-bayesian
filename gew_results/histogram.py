import csv
from os import listdir

# First get all the data from the files.

sequences_filename = "SEQUENCES.csv"

with open(sequences_filename) as f:
    csv_reader = csv.reader(f, delimiter=',')
    line_count = 0
    sequences = ['']*50
    for row in csv_reader:
        if line_count != 0:
            sequences[int(row[0])] = row[1]
        line_count += 1

files = [x for x in listdir('.') if '2019' in x]

file_count = 0

data = [{}] * 50
for filename in files:
    training_set = []
    emotion_name = []
    emotion_number = []
    intensity = []
    shown_emotion = []
    participant_data = {}
    with open(filename) as f:
        csv_reader = csv.reader(f, delimiter=',')
        line_count = 0
        test_id = -1
        for row in csv_reader:
            if line_count != 0:
                if test_id == -1:
                    test_id = int(row[0])
                if test_id == 12:
                    print("Skipping id "+str(test_id))
                    break
                if row[1] == '1':
                    # Training element
                    shown_emotion += ['t'+str(line_count)]
                    training_set += [True]
                else:
                    training_set += [False]
                    shown_emotion += [sequences[test_id][line_count-8]]

                emotion_name += [row[2]]
                emotion_number += [row[7]]
                intensity += [row[3]]

            line_count += 1

        participant_data['training_set'] = training_set
        participant_data['emotion_name'] = emotion_name
        participant_data['emotion_number'] = emotion_number
        participant_data['intensity'] = intensity
        participant_data['shown_emotion'] = shown_emotion
        participant_data['id'] = test_id

    data[test_id] = participant_data
    file_count += 1

# Now we have the 'data' structure.
# Example use (for participant ID 7, and 8th shown emotion):
# print(data[7]['shown_emotion'][8])

# So now we go over the emotions, and group data together accordingly.

emotion = {}
for e in range(10):
    emotion["emotion_0"+str(e)+"_hist.csv"] = [0]*121


print(emotion)
for participant in data:  # For each participant
    if len(participant) != 0 and len(participant['intensity']) != 0:  # Check for excluded data
        for i in range(len(participant['intensity'])):  # For each line of the participant's data
            if not participant['training_set'][i]:  # If it's not training data
                try:
                    emid = int(participant['shown_emotion'][i])
                    emo = int(participant['emotion_number'][i])
                    inty = int(participant['intensity'][i])

                    if inty > 1:
                        emotion["emotion_0"+str(emid)+"_hist.csv"][20*(inty-1)+emo] += 1
                        #print("Emotion "+str(emo)+" intensity "+str(inty)+": segment "+str(20*(inty-1)+emo))
                    else:
                        for i in range(1,21):
                            emotion["emotion_0"+str(emid)+"_hist.csv"][i] += 1

                except ValueError:
                    pass
                    #print("ValueError on line "+str(i)+" of participant "+str(participant['id']))
                    #print("emotion_names = "+str(participant['emotion_name']))


print(emotion)

# Now we have all the emotion data grouped together.
# Just have to export the whole thing as csv files

for fn, v in emotion.items():

    new_file_contents = ""

    for i in range(1,121):
        new_file_contents += str(i)+","+str(v[i])+"\n"

    f = open("segment_histograms/"+fn,"w+")
    f.write(new_file_contents)
    f.close()
